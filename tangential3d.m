model = createpde;
load ballmesh.mat;
geometryFromMesh(model,p',t');
applyBoundaryCondition(model,'dirichlet','Face',...
    1:model.Geometry.NumFaces,'r',@tfuncx);
specifyCoefficients(model,'m',0,'d',0,'c',1,'a',0,'f',0);
btx = solvepde(model);
xt = btx.NodalSolution;
applyBoundaryCondition(model,'dirichlet','Face',...
    1:model.Geometry.NumFaces,'r',@tfuncy);
bty = solvepde(model);
yt = bty.NodalSolution;
%{
applyBoundaryCondition(model,'dirichlet','Face',...
    1:model.Geometry.NumFaces,'r', cos(pi/2));
btz = solvepde(model);
zt = btz.NodalSolution;
phi = pi/2;
%}
applyBoundaryCondition(model,'dirichlet','Face',...
    1:model.Geometry.NumFaces,'r',@nfuncx);
specifyCoefficients(model,'m',0,'d',0,'c',1,'a',0,'f',0);
bnx = solvepde(model);
xn = bnx.NodalSolution;
applyBoundaryCondition(model,'dirichlet','Face',...
    1:model.Geometry.NumFaces,'r',@nfuncy);
bny = solvepde(model);
yn = bny.NodalSolution;
applyBoundaryCondition(model,'dirichlet','Face',...
    1:model.Geometry.NumFaces,'r', @nfuncz);
bnz = solvepde(model);
zn = bnz.NodalSolution;

thetat = mod(atan2(yt,xt),2*pi);
thetan = mod(atan2(yn,xn),2*pi);
phin = acos(zn);
x = cos(thetan).*sin(phin);
y = sin(thetan).*sin(phin);
z = cos(phin);
figure
%quiver3(p(:,1),p(:,2),p(:,3),cos(thetat),sin(thetat),zeros(size(thetat)));
quiver3(p(:,1),p(:,2),p(:,3),x,y,z);
axis equal
xlabel('x')
ylabel('y')
zlabel('z')

TR = triangulation(t,p);

function bcMatrix = tfuncx(region,~)
x = region.x;
y = region.y;
%tangent vector [-y,x,0]
theta = mod(atan2(x,-y),pi/2)*4;
if abs(theta-2*pi)<1e-6
    theta = 0;
end
phi = pi/2;
bcMatrix = cos(theta)*sin(phi);
end

function bcMatrix = tfuncy(region,~)
x = region.x;
y = region.y;
%tangent vector [-y,x,0]
theta = mod(atan2(x,-y),pi/2)*4;
if abs(theta-2*pi)<1e-6
    theta = 0;
end
phi = pi/2;
bcMatrix = sin(theta)*sin(phi);
end

function bcMatrix = nfuncx(region,~)
x = region.x;
y = region.y;
z = region.z;
%normal vector [x,y,z]
theta = mod(atan2(y,x),pi/2)*4;
if abs(theta-2*pi)<1e-6
    theta = 0;
end
phi = mod(acos(z),pi/2)*4;
if abs(phi-2*pi)<1e-6
    phi = 0;
end
bcMatrix = cos(theta)*sin(phi);
end

function bcMatrix = nfuncy(region,~)
x = region.x;
y = region.y;
z = region.z;
%normal vector [x,y,z]
theta = mod(atan2(y,x),pi/2)*4;
if abs(theta-2*pi)<1e-6
    theta = 0;
end
phi = mod(acos(z),pi/2)*4;
if abs(phi-2*pi)<1e-6
    phi = 0;
end
bcMatrix = sin(theta)*sin(phi);
end

function bcMatrix = nfuncz(region,~)
z = region.z;
%normal vector [x,y,z]
phi = mod(acos(z),pi/2)*4;
if abs(phi-2*pi)<1e-6
    phi = 0;
end
bcMatrix = cos(phi);
end
