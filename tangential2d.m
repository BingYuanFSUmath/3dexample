model = createpde;
geometryFromEdges(model,@circleg);
applyBoundaryCondition(model,'dirichlet','Edge',...
    1:model.Geometry.NumEdges,'r',@cxfunc);
specifyCoefficients(model,'m',0,'d',0,'c',1,'a',0,'f',0);
generateMesh(model,'Hmax',0.1);
cx = solvepde(model);
x = cx.NodalSolution;
%x = mod(atan2(imag(x),real(x)),2*pi);%convert to angle [0,2pi]
%x = mod(x,2*pi);
applyBoundaryCondition(model,'dirichlet','Edge',...
    1:model.Geometry.NumEdges,'r',@cyfunc);
cy = solvepde(model);
y = cy.NodalSolution;
t = model.Mesh.Elements';
p = model.Mesh.Nodes';
TR = triangulation(t,p);
N = Node(TR); %initialize Nodes
for i = 1:size(N,1)
%N(i).d = x(i);
N(i).d = mod(atan2(y(i),x(i)),2*pi);
end
S = Singularity;
S = find_S_new(S,N,TR);
S = S(1,2:end); %remove the initial zero
plot_vector_field(N,S,TR,1)
save cx2d.mat cx


function bcMatrix = cxfunc(region,~)
x = region.x;
y = region.y;
%tangent vector [-y,x]
cross = mod(atan2(x,-y),pi/2);
d =cross*4;
if abs(d-2*pi)<1e-6
    d = 0;
end
bcMatrix = cos(d);
%bcMatrix = complex(cos(d),sin(d));
end

function bcMatrix = cyfunc(region,~)
x = region.x;
y = region.y;
%tangent vector [-y,x]
cross = mod(atan2(x,-y),pi/2);
d =cross*4;
if abs(d-2*pi)<1e-6
    d = 0;
end
bcMatrix = sin(d);
%bcMatrix = complex(cos(d),sin(d));
end

